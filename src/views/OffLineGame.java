package views;

import game.Window;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Stack;
import javax.swing.Timer;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import tools.ChessBoard;

public class OffLineGame extends JPanel implements ActionListener, MouseListener {
	
	JButton regret;			// 悔棋 按钮
	JButton exit;			// 离开 按钮
	
	JLabel showChess;			// 显示执什么棋子
	JLabel showBlackCnt;		// 显示黑棋数量
	JLabel showWhiteCnt;		// 显示白棋数量
	JLabel showStatus;			// 显示当前谁的回合
	
	int blackCnt = 0;
	int whiteCnt = 0;
	
	ChessBoard board;		// 棋盘数据结构
	
	boolean enable;				// 使能信号，如果为true则轮到自身下棋，否则为对方下棋
	boolean isBlack;			// 是否执黑棋，执黑棋为先手
	boolean gameIsOver = false;	// 游戏是否结束
	
	private int myLastX = -1, myLastY = -1, computerLastX = -1, computerLastY = -1;
	
	private Stack<Integer> myLast = new Stack<Integer>();
	private Stack<Integer> computerLast = new Stack<Integer>();
	
	int distinceOfX = 30;  	// 距离 x 边界
	int distinceOfY = 30; 	// 距离 y 边界
	int widthOfLatic = 43; 	// 格宽
	
	private String[] blackKey = new String[]{
			"XXXXX"," XXXX "," XXXX","XXXX ","X XXX","XX XX","XXX X"," XXX ","XXX "," XXX","X XX","XX X"," XX ","XX","X"
	};
	private String[] whiteKey = new String[]{
			"OOOOO"," OOOO "," OOOO","OOOO ","O OOO","OO OO","OOO O"," OOO ","OOO "," OOO","O OO","OO O"," OO ","OO","O"
	};
	private int[] keyValues = new int[]{
		100,90,80,80,80,80,80,70,60,50,40,30,20,10,5
	};
	
	char comCurrent; // 电脑棋
	char humCurrent; // 玩家棋
	
	Window window = null;
	
	public OffLineGame(Window win, boolean isFirst){
		
		window = win;
		isBlack = isFirst;
		enable = isBlack;
		board = new ChessBoard();	
		
		if(this.isBlack) {
			comCurrent = board.White_Char; // 电脑为白
			humCurrent = board.Black_Char;
		}else {
			comCurrent = board.Black_Char; // 电脑为黑
			humCurrent = board.White_Char;
		}
		
		this.setSize(950, 662);										// 设置面板大小
		this.addMouseListener(this);								// 为面板加入监听
		
		this.setLayout(null);		// 设置为空布局
		this.setBackground(Color.WHITE);
		
		regret = new JButton("悔棋");
		regret.setFont(new Font("黑体", Font.BOLD, 26));
		regret.setBackground(new Color(0xffededed));
		regret.setFocusPainted(false);
		regret.setBounds(662, 320, 270, 70);
		regret.addActionListener(this);
		this.add(regret);
		
		exit = new JButton("离开");
		exit.setFont(new Font("黑体", Font.BOLD, 26));
		exit.setBackground(new Color(0xffededed));
		exit.setFocusPainted(false);
		exit.setBounds(662, 548, 270, 70);
		exit.addActionListener(this);
		this.add(exit);
		
		//添加 显示时间的JLabel
		JLabel time = new JLabel();
		time.setFont(new Font("黑体", Font.BOLD, 40));
		time.setBounds(780, 30, 300, 50);
		this.add(time);
		this.setTimer(time);
		
		showChess = new JLabel( String.format("执棋：%s", isBlack ? "黑子" : "白子") );
		showChess.setFont(new Font("黑体", Font.BOLD, 26));
		showChess.setBounds(662, 116, 300, 30);
		this.add(showChess);
		
		showBlackCnt = new JLabel("");
		showBlackCnt.setFont(new Font("黑体", Font.BOLD, 26));
		showBlackCnt.setBounds(662, 159, 300, 30);
		this.add(showBlackCnt);
		
		showWhiteCnt = new JLabel("");
		showWhiteCnt.setFont(new Font("黑体", Font.BOLD, 26));
		showWhiteCnt.setBounds(662, 202, 300, 30);
		this.add(showWhiteCnt);
		
		showStatus = new JLabel("");
		showStatus.setFont(new Font("黑体", Font.BOLD, 26));
		showStatus.setBounds(662, 245, 300, 30);
		this.add(showStatus);
		
		new Thread(new Runnable() { public void run() { go(); } }).start();
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				while (gameIsOver == false) {
					try{ Thread.sleep(30); } catch (Exception E) { };
					showBlackCnt.setText( String.format("黑子：%d", blackCnt) );
					showWhiteCnt.setText( String.format("白子：%d", whiteCnt) );
					showStatus.setText( String.format("当前状态：%s回合", enable ? "我方" : "对方") );
				}
			}
		
		}).start();
	}
	
	// 设置Timer 1000ms实现一次动作 实际是一个线程
	private void setTimer(JLabel time) {
		final JLabel varTime = time;
		long startTime = System.currentTimeMillis();
		Timer timeAction = new Timer(1000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				long timemillis = System.currentTimeMillis();
				// 转换日期显示格式
				SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
				varTime.setText(df.format(new Date(timemillis - startTime - 28800000)));
			}
		});
		timeAction.start();
	}
	
	public void go() {
		if (enable == false) { // 电脑先走
			JOptionPane.showMessageDialog(window, "电脑为先手");
			try { Thread.sleep(500); } catch (Exception e) { };
			board.placeChess(comCurrent, 7, 7);
			this.computerLast.push(7);
			this.computerLast.push(7);
			if (comCurrent == ChessBoard.Black_Char) blackCnt ++;
			else whiteCnt ++;
			repaint();
			enable = true;
		}
		else {
			JOptionPane.showMessageDialog(window, "玩家为先手");
		}
	}
	
	public void computerPlayer(){
		int tempRow = -1, tempCol = -1, maxValue = 0;
			
		for (int i = 0; i < board.M; i ++) {
			for (int j = 0; j < board.M; j ++) {
				if (!board.posIsBlank(i, j)) continue;
				int attack = checkMax(i, j, comCurrent); // 计算攻击评估
				int defend = checkMax(i, j, humCurrent); // 防守指数
				int max = Math.max(attack, defend);
				if(max > maxValue) {
					tempRow = i;
					tempCol = j;
					maxValue = max;
				}
			}
		}
		
		//放子
		board.placeChess(comCurrent, tempRow, tempCol);
		computerLastX = tempRow;
		computerLastY = tempCol;
		computerLast.push(tempRow);
		computerLast.push(tempCol);
		
		if (comCurrent == ChessBoard.Black_Char) blackCnt ++;
		else whiteCnt ++;
		
		repaint();
		
		if (board.canWin(comCurrent, tempRow, tempCol)) {
			gameIsOver = true;
			int choice = JOptionPane.showConfirmDialog(window, "失败！失败！再来一局？", "系统消息", JOptionPane.YES_NO_OPTION);
			if (choice == 0) {
				int randInt = (int)(0 + Math.random() * (1 - 0 + 1));
				boolean humanFirst = (randInt == 1) ? true : false;
				window.setCurrentPanel(new OffLineGame(window, humanFirst));
			}
		}
	}
	

	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		int x = e.getX();
		int y = e.getY();
		
		if(gameIsOver == false && enable && x >= distinceOfX && x <= widthOfLatic*14+distinceOfX+3 && y >= distinceOfY && y <= widthOfLatic*14 + distinceOfY+3){
			double tempx = (x - distinceOfX) / (1.0 * widthOfLatic);
			double tempy = (y - distinceOfY) / (1.0 * widthOfLatic);
		
			x = (int )(tempx + 0.5);
			y = (int )(tempy + 0.5);
			
			if(board.posIsBlank(x, y)) {
				
				myLast.push(x);
				myLast.push(y);
				
				board.placeChess(humCurrent, x, y);
				
				if (humCurrent == ChessBoard.Black_Char) blackCnt ++;
				else whiteCnt ++;
				
				this.paintImmediately(0, 0, 950, 662);
				
				enable =  false;
				
				if (board.canWin(humCurrent, x, y)) {
					gameIsOver = true;
					int choice = JOptionPane.showConfirmDialog(window, "游戏胜利！再来一局？", "系统消息", JOptionPane.YES_NO_OPTION);
					if (choice == 0) {
						int randInt = (int)(0 + Math.random() * (1 - 0 + 1));
						boolean humanFirst = (randInt == 1) ? true : false;
						window.setCurrentPanel(new OffLineGame(window, humanFirst));
					}
				}
				if(gameIsOver != true){
					try{ Thread.sleep(500); } catch (Exception e1) { };
					computerPlayer();  // 电脑下棋
				}
				enable = true;
			}
			
		}
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getActionCommand() == "悔棋") {
			if (gameIsOver == false) {
				if (this.enable && !myLast.empty()) {
					myLastY = myLast.pop();
					myLastX = myLast.pop();
					board.placeChess(board.Blank_Char, myLastX, myLastY); // 悔棋人类
	
					computerLastY = computerLast.pop();
					computerLastX = computerLast.pop();
					board.placeChess(board.Blank_Char, computerLastX, computerLastY); // 悔棋电脑
					
					blackCnt --;
					whiteCnt --;
					
					this.repaint();
				}else {
					JOptionPane.showMessageDialog(this, "现在还不能悔棋");
				}
			}
			else {
				JOptionPane.showMessageDialog(window, "游戏已经结束，后悔也没有用了");
			}
		}
		else if (e.getActionCommand() == "离开") {
			window.setCurrentPanel(new Welcome(window));
		}
	}
	

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		ImageIcon icon = new ImageIcon("src/image/background2.png");
		Image img = icon.getImage();
		g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		
		g.setColor(Color.BLACK);
		for(int i=0; i<15; i++) {
			g.drawLine(distinceOfX, distinceOfY+widthOfLatic*i, widthOfLatic*14+distinceOfX, distinceOfY+widthOfLatic*i);
			g.drawLine(distinceOfX+widthOfLatic*i, distinceOfY, distinceOfX+widthOfLatic*i, widthOfLatic*14 + distinceOfY);
		}
		
		g.fillOval(154, 154, 10, 10);
		g.fillOval(500, 154, 10, 10);
		g.fillOval(154, 498, 10, 10);
		g.fillOval(498, 498, 10, 10);
		g.fillOval(326, 326, 10, 10);
		
		for (int i = 0; i < 15; i++){
			for (int j = 0; j < 15; j++){
				if (board.whichChessInThisPos(i, j) == ChessBoard.Black_Char){		// 画黑子
					int tempX = i*widthOfLatic + distinceOfX;
					int tempY = j*widthOfLatic + distinceOfY;
					g.setColor(Color.BLACK);
					g.fillOval(tempX-10, tempY-10, 25, 25);
				}
				if (board.whichChessInThisPos(i, j) == ChessBoard.White_Char){		// 画白子
					int tempX = i*widthOfLatic + distinceOfX;
					int tempY = j*widthOfLatic + distinceOfY;
					g.setColor(Color.WHITE);
					g.fillOval(tempX-10, tempY-10, 25, 25);
					g.setColor(Color.black);
					g.drawOval(tempX-10, tempY-10, 25, 25);
				}
			}
		}
		
		if(!this.computerLast.empty()){
			int thisPosY = this.computerLast.pop();
			int thisPosX = this.computerLast.pop();
			
			this.computerLast.push(thisPosX);
			this.computerLast.push(thisPosY);
			
			int tempX = thisPosX*widthOfLatic + distinceOfX;
			int tempY = thisPosY*widthOfLatic + distinceOfY;
			//if()
			g.setColor(Color.LIGHT_GRAY);
			g.fillOval(tempX-2, tempY-2, 10, 10);
		}
		
	}
	
	
	//评估函数
	private int checkMax(int row, int col, char current) {
		int max = 0;
		int tmpMax = 0;
		String[] array;
		if(current == board.Black_Char)
			array = blackKey;
		else
			array = whiteKey;
		StringBuilder builder = new StringBuilder();
		//水平方向检测评估
		for (int i=-4; i<=4; i++) {
			int newCol = col + i;
			if(newCol <0 || newCol >= board.M) continue;
			if( i==0 ){
				builder.append(current);
			}else {
				builder.append(board.whichChessInThisPos(row, newCol));
			}
		}
		for (int i=0; i<array.length; i++){
			String key = array[i];
			if (builder.indexOf(key) >= 0){
				tmpMax = keyValues[i];
				break;
			}
		}
		if(tmpMax > max)
			max = tmpMax;
		
		if(tmpMax == 100)
			return tmpMax;
		
		//竖直方向
		builder.delete(0, builder.length());
		for (int i=-4; i<=4; i++) {
			int newRow = row + i;
			if(newRow <0 || newRow >= board.M) continue;
			if( i==0 ){
				builder.append(current);
			}else {
				builder.append(board.whichChessInThisPos(newRow, col));
			}
		}
		for (int i=0; i<array.length; i++){
			String key = array[i];
			if (builder.indexOf(key) >= 0){
				tmpMax = keyValues[i];
				break;
			}
		}
		
		if(tmpMax > max)
			max = tmpMax;
		
		if(tmpMax == 100)
			return tmpMax;
		
		//右下 左上 45度
		builder.delete(0, builder.length());
		for (int i=-4; i<=4; i++) {
			int newRow = row + i;
			int newCol = col + i;
			if(newRow <0 ||newCol <0||newCol>=board.M|| newRow >= board.M) continue;
			if( i==0 ){
				builder.append(current);
			}else {
				builder.append(board.whichChessInThisPos(newRow, newCol));
			}
		}
		for (int i=0; i<array.length; i++){
			String key = array[i];
			if (builder.indexOf(key) >= 0){
				tmpMax = keyValues[i];
				break;
			}
		}
		
		if(tmpMax > max)
			max = tmpMax;
		
		if(tmpMax == 100)
			return tmpMax;
		
		
		// 右上 左下 45度
		builder.delete(0, builder.length());
		for (int i=-4; i<=4; i++) {
			int newRow = row - i;
			int newCol = col + i;
			if(newRow <0 ||newCol <0||newCol>=board.M|| newRow >= board.M) continue;
			if( i==0 ){
				builder.append(current);
			}else {
				builder.append(board.whichChessInThisPos(newRow, newCol));
			}
		}
		for (int i=0; i<array.length; i++){
			String key = array[i];
			if (builder.indexOf(key) >= 0){
				tmpMax = keyValues[i];
				break;
			}
		}
		
		if(tmpMax > max)
			max = tmpMax;
		
		return max;
	}
		
}
