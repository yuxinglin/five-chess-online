package views;

import tools.*;
import game.Window;

import java.net.InetAddress;
import java.net.ServerSocket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Room extends JPanel implements ActionListener {

	NetWork net;		// 联机和收发消息的工具
						// 0,xxxx,0     普通信息
						// 1,xxxx,0   一般控制信息
						// 1,游戏开始,0/1	游戏开始控制信息，0/1代表是否为先手
	
	JTextArea show;		// 显示消息
	JTextField input;		// 输入信息
	
	JButton exit;			// 离开 按钮
	JButton ready;			// 准备 按钮
	JButton cancelReady;	// 取消准备 按钮
	JButton send;			// 发送 按钮
	
	boolean gameStart = false;
	
	Window window = null;
	
	public Room(Window win, NetWork paramNet) {
		
		window = win;
		net = paramNet;
		
		this.setSize(950, 662);										// 设置面板大小
		this.setLayout(null);
		this.setBackground(Color.WHITE);
		
		JLabel label1 = new JLabel("房间信息");
		label1.setFont(new Font("黑体", Font.BOLD, 30));
		label1.setBounds(50, 150, 300, 50);
		this.add(label1);
		
		JLabel label2 = new JLabel(">");
		label2.setFont(new Font("黑体", Font.BOLD, 30));
		label2.setBounds(30, 595, 30, 30);
		this.add(label2);
		
		JPanel p1 = new JPanel(new BorderLayout());
		p1.setBounds(50, 200, 600, 370);
		show = new JTextArea();
		show.setFont(new Font("宋体", Font.BOLD, 22));
		show.setEditable(false);						// 设置文本区不可编辑
		show.setLineWrap(true);        					// 激活自动换行功能
		show.setWrapStyleWord(true);            		// 激活断行不断字功能
		JScrollPane scroll = new JScrollPane(show);		// 添加滚动条
		p1.add(scroll, BorderLayout.CENTER);
		this.add(p1);
		
		input = new JTextField(50);
		input.setFont(new Font("宋体", Font.BOLD, 22));
		input.setBounds(50, 590, 600, 45);
		input.setText("输入信息后点击发送进行聊天");
		this.add(input);
		
		exit = new JButton("离开");
		exit.setBounds(680, 200, 260, 70);
		exit.setFocusPainted(false);
		exit.setFont(new Font("黑体", Font.BOLD, 30));
		exit.setBackground(new Color(0xffededed));
		exit.addActionListener(this);
		this.add(exit);
		
		ready = new JButton("准备");
		ready.setBounds(680, 310, 260, 70);
		ready.setFocusPainted(false);
		ready.setFont(new Font("黑体", Font.BOLD, 30));
		ready.setBackground(new Color(0xffededed));
		ready.addActionListener(this);
		this.add(ready);
		
		cancelReady = new JButton("取消准备");
		cancelReady.setBounds(680, 420, 260, 70);
		cancelReady.setFocusPainted(false);
		cancelReady.setFont(new Font("黑体", Font.BOLD, 30));
		cancelReady.setBackground(new Color(0xffededed));
		cancelReady.addActionListener(this);
		this.add(cancelReady);
		cancelReady.setEnabled(false);
		
		send = new JButton("发送");
		send.setBounds(680, 530, 260, 70);
		send.setFocusPainted(false);
		send.setFont(new Font("黑体", Font.BOLD, 30));
		send.setBackground(new Color(0xffededed));
		send.addActionListener(this);
		this.add(send);
		
		show.append("已进入房间\n");
		show.append("--------------------------------\n");
		
		// 开启线程用于接收房主开始游戏的消息
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				while (!gameStart) {
					try{ Thread.sleep(30); } catch (Exception e) { };
					String []info = net.receive().split(",");
					if (info[0].equals("1")) {
						if (info[1].equals("房主已离开")) {
							show.append("[系统消息]" + info[1] + "\n");
							show.setCaretPosition(show.getDocument().getLength());
							net.close();
							break;
						}
						else if (info[1].equals("游戏开始")) {
							net.send("1,确认游戏开始,0");
							show.append("[系统消息]" + info[1] + "\n");
							show.setCaretPosition(show.getDocument().getLength());
							window.setCurrentPanel(new GameView(window, net, info[2].equals("1") ? true : false) );
							gameStart = true;
							break;
						}
					}
					else if (info[0].equals("0")) {
						show.append("-- " + info[1] + "\n");
						show.setCaretPosition(show.getDocument().getLength());
					}
				}
			}
			
		}).start();
		
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		ImageIcon icon = new ImageIcon("src/image/background1.png");
		Image img = icon.getImage();
		g.drawImage(img, 0, 0, 1290, 722, this);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getActionCommand() == "离开") {
			net.send("1,玩家已离开,0");
			net.close();
			window.setCurrentPanel(new EnterRoomView(window));
		}
		else if (e.getActionCommand() == "准备") {
			net.send("1,玩家已准备,0");
			show.append("[系统消息]已准备\n");
			show.setCaretPosition(show.getDocument().getLength());
			ready.setEnabled(false);
			cancelReady.setEnabled(true);
		}
		else if (e.getActionCommand() == "取消准备") {
			net.send("1,玩家取消准备,0");
			show.append("[系统消息]已取消准备\n");
			show.setCaretPosition(show.getDocument().getLength());
			ready.setEnabled(true);
			cancelReady.setEnabled(false);
		}
		else if (e.getActionCommand() == "发送") {
			String str = input.getText();
			if (!str.equals("")) {
				input.setText("");
				net.send("0," + str + ",0");
				show.append(">> " + str + "\n");
				show.setCaretPosition(show.getDocument().getLength());
			}
		}
	}

}
