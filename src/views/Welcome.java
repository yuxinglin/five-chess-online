package views;

import game.Description;
import game.Window;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Welcome extends JPanel implements ActionListener {

	JButton enterRoom;		// 进入房间
	JButton createRoom;		// 创建房间
	JButton offLine;		// 单机游戏
	JButton explain;		// 说明
	
	Window window = null;
	
	public Welcome(Window win) {
		
		this.window = win;
		
		this.setSize(950, 662);										// 设置面板大小
		this.setLayout(null);
		this.setBackground(Color.WHITE);
		
		this.enterRoom = new JButton("进入房间");
		this.enterRoom.setFocusPainted(false);
		this.enterRoom.setFont(new Font("黑体", Font.BOLD, 35));
		this.enterRoom.setBackground(new Color(0xffededed));
		this.enterRoom.setBounds(530, 300, 400, 90);
		this.enterRoom.addActionListener(this);
		
		this.createRoom = new JButton("创建房间");
		this.createRoom.setFocusPainted(false);
		this.createRoom.setFont(new Font("黑体", Font.BOLD, 35));
		this.createRoom.setBackground(new Color(0xffededed));
		this.createRoom.setBounds(530, 420, 400, 90);
		this.createRoom.addActionListener(this);
		
		this.offLine = new JButton("单机游戏");
		this.offLine.setFocusPainted(false);
		this.offLine.setFont(new Font("黑体", Font.BOLD, 35));
		this.offLine.setBackground(new Color(0xffededed));
		this.offLine.setBounds(530, 540, 400, 90);
		this.offLine.addActionListener(this);
		
		this.explain = new JButton("说明");
		this.explain.setFocusPainted(false);
		this.explain.setFont(new Font("黑体", Font.BOLD, 26));
		this.explain.setContentAreaFilled(false);				//设置按钮透明
//		this.explain.setBackground(Color.LIGHT_GRAY);
		this.explain.setBounds(50, 590, 200, 40);
		this.explain.addActionListener(this);
		
		this.add(this.enterRoom);
		this.add(this.createRoom);
		this.add(this.offLine);
		this.add(this.explain);
		
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		ImageIcon icon = new ImageIcon("src/image/background1.png");
		Image img = icon.getImage();
		g.drawImage(img, 0, 0, 1290, 722, this);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getActionCommand() == "进入房间") {
			window.setCurrentPanel(new EnterRoomView(window));
		}
		else if (e.getActionCommand() == "创建房间") {
			window.setCurrentPanel(new HostRoom(window));
		}
		else if (e.getActionCommand() == "单机游戏") {
			int randInt = (int)(0 + Math.random() * (1 - 0 + 1));
			boolean humanFirst = (randInt == 1) ? true : false;
			window.setCurrentPanel(new OffLineGame(window, humanFirst));
		}
		else if (e.getActionCommand() == "说明") {
			new Description();
		}
	}
	
}
