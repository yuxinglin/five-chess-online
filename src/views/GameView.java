package views;

import tools.*;
import game.Window;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Stack;
import java.util.Date;

public class GameView extends JPanel implements ActionListener, MouseListener {
	
	NetWork net;			// 联机和收发消息的工具	
							// 消息格式为a,b,c    a为1代表此消息为请求悔棋，a为0代表此消息为坐标(b, c)
							// 1,0,0:请求悔棋	1,0,1:拒绝悔棋	1,1,0:同意悔棋
							// 2,0,0:请求和棋       2,0,1:拒绝和棋      2,1,0:同意和棋
							// 3,0,0:投降
	
	JButton regret;			// 悔棋 按钮
	JButton requestDraw;	// 请和 按钮
	JButton surrender;		// 投降 按钮
	JButton back;			// 返回 按钮
	
	JLabel showChess;			// 显示执什么棋子
	JLabel showBlackCnt;		// 显示黑棋数量
	JLabel showWhiteCnt;		// 显示白棋数量
	JLabel showStatus;			// 显示当前谁的回合
	
	int blackCnt = 0;
	int whiteCnt = 0;
	
	ChessBoard board;		// 棋盘数据结构
	
	boolean enable;				// 使能信号，如果为true则轮到自身下棋，否则为对方下棋
	boolean isBlack;			// 是否执黑棋，执黑棋为先手
	boolean gameIsOver = false;	// 游戏是否结束
	
	private int myLastX = -1, myLastY = -1, urLastX = -1, urLastY = -1;
	
	BufferedImage bgImage = null;			//初始化背景图片
	
	int distinceOfX = 30;  	// 距离 x 边界
	int distinceOfY = 30; 	// 距离 y 边界
	int widthOfLatic = 43; 	// 格宽
	
	Window window = null;
	
	public GameView(Window win, NetWork paramNet, boolean isFirst) {
		
		window = win;
		net = paramNet;
		enable = isFirst;
		isBlack = isFirst;
		
		board = new ChessBoard();									// 创建棋盘数据结构
		this.setSize(950, 662);										// 设置面板大小
		this.addMouseListener(this);								// 为面板加入监听
		
		this.setLayout(null);		// 设置为空布局
		this.setBackground(Color.WHITE);
		
		regret = new JButton("悔棋");
		regret.setFont(new Font("黑体", Font.BOLD, 26));
		regret.setBackground(new Color(0xffededed));
		regret.setFocusPainted(false);
		regret.setBounds(662, 320, 270, 70);
		regret.addActionListener(this);
		this.add(regret);
		
		requestDraw = new JButton("请和");
		requestDraw.setFont(new Font("黑体", Font.BOLD, 26));
		requestDraw.setBackground(new Color(0xffededed));
		requestDraw.setFocusPainted(false);
		requestDraw.setBounds(662, 434, 270, 70);
		requestDraw.addActionListener(this);
		this.add(requestDraw);
		
		surrender = new JButton("投降");
		surrender.setFont(new Font("黑体", Font.BOLD, 26));
		surrender.setBackground(new Color(0xffededed));
		surrender.setFocusPainted(false);
		surrender.setBounds(662, 548, 270, 70);
		surrender.addActionListener(this);
		this.add(surrender);
		
		back = new JButton("返回");
		back.setFont(new Font("黑体", Font.BOLD, 16));
		back.setBackground(new Color(0xffededed));
		back.setFocusPainted(false);
		back.setBounds(660, 30, 110, 40);
		back.addActionListener(this);
		this.add(back);
		back.setEnabled(false);
		
		//添加 显示时间的JLabel
		JLabel time = new JLabel();
		time.setFont(new Font("黑体", Font.BOLD, 40));
		time.setBounds(780, 45, 300, 50);
		this.add(time);
		this.setTimer(time);
		
		showChess = new JLabel( String.format("执棋：%s", isBlack ? "黑子" : "白子") );
		showChess.setFont(new Font("黑体", Font.BOLD, 26));
		showChess.setBounds(662, 116, 300, 30);
		this.add(showChess);
		
		showBlackCnt = new JLabel("");
		showBlackCnt.setFont(new Font("黑体", Font.BOLD, 26));
		showBlackCnt.setBounds(662, 159, 300, 30);
		this.add(showBlackCnt);
		
		showWhiteCnt = new JLabel("");
		showWhiteCnt.setFont(new Font("黑体", Font.BOLD, 26));
		showWhiteCnt.setBounds(662, 202, 300, 30);
		this.add(showWhiteCnt);
		
		showStatus = new JLabel("");
		showStatus.setFont(new Font("黑体", Font.BOLD, 26));
		showStatus.setBounds(662, 245, 300, 30);
		this.add(showStatus);
		
		// 开启线程控制组件是否可用
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try{
						Thread.sleep(30);
					}catch(Exception e){ };
					
					regret.setEnabled(enable);
					requestDraw.setEnabled(enable);
					surrender.setEnabled(enable);
					back.setEnabled(gameIsOver);
					
				}
			}
			
		}).start();
		
		// 开启线程收消息
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (gameIsOver == false) {
					try { Thread.sleep(30); } catch (Exception e) { };
					if (enable == false) {
						String []info = net.receive().split(",");
						int a = new Integer(info[0]);
						int b = new Integer(info[1]);
						int c = new Integer(info[2]);
						if (a == 0) {
							// 此为棋盘位置坐标
							urLastX = b;
							urLastY = c;
							char chess = isBlack ? ChessBoard.White_Char : ChessBoard.Black_Char;
							board.placeChess(chess, b, c);
							
							if (chess == ChessBoard.Black_Char) blackCnt ++;
							else	whiteCnt ++;
							
							// 设置enable
							enable = true;
							repaint();
							// 判断游戏是否结束
							if (board.canWin(chess, b, c)) {
								gameIsOver = true;
								JOptionPane.showMessageDialog(window, "失败！失败！");
							}
						}
						else if (a == 1) {
						// 此为悔棋消息
							if (b == 0 && c == 0) {
							// 此为对方请求悔棋
								Object[] options ={ "同意", "拒绝" };  //自定义按钮上的文字
								int m = JOptionPane.showOptionDialog(window, "对方请求悔棋", "系统消息", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
								if (m == 0) { // 同意
									net.send("1,1,0");
									board.unPlace(myLastX, myLastY);
									board.unPlace(urLastX, urLastY);
									myLastX = -1;
									myLastY = -1;
									urLastX = -1;
									urLastY = -1;
									blackCnt --;
									whiteCnt --;
									repaint();
								}
								else {			// 拒绝
									net.send("1,0,1");
								}
							}
							else if (b == 0 && c == 1) {
							// 此为对方拒绝悔棋
								JOptionPane.showMessageDialog(window, "对方拒绝悔棋");
								enable = true;
							}
							else if (b == 1 && c == 0) {
							// 此为对方同意悔棋
								board.unPlace(myLastX, myLastY);
								board.unPlace(urLastX, urLastY);
								myLastX = -1;
								myLastY = -1;
								urLastX = -1;
								urLastY = -1;
								blackCnt --;
								whiteCnt --;
								enable = true;
								repaint();
							}
							else {
							// 此为错误消息
								
							}
						}
						else if (a == 2) {
							if (b == 0 && c == 0) {			// 对方请求和棋
								Object[] options ={ "同意", "拒绝" };  //自定义按钮上的文字
								int m = JOptionPane.showOptionDialog(window, "对方请求和棋", "系统消息", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
								if (m == 0) {
									net.send("2,1,0");
									gameIsOver = true;
									JOptionPane.showMessageDialog(window, "和棋：平局！");
								}
								else {
									net.send("2,0,1");
								}
							}
							else if (b == 0 && c == 1) {	// 对方拒绝和棋
								JOptionPane.showMessageDialog(window, "对方拒绝和棋");
								enable = true;
							}
							else if (b == 1 && c == 0) {	// 对方同意和棋
								JOptionPane.showMessageDialog(window, "对方同意和棋：平局！");
								gameIsOver = true;
							}
							else {}
						}
						else if (a == 3) {
							if (b == 0 && c == 0) {			// 对方投降
								JOptionPane.showMessageDialog(window, "对方已投降：胜利！");
								gameIsOver = true;
							}
							else {}
						}
						else {}
					}
				}
			}
			
		}).start();
		
		// 开启线程实时显示当前棋盘状态信息
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				while (gameIsOver == false) {
					try{ Thread.sleep(30); } catch (Exception E) { };
					showBlackCnt.setText( String.format("黑子：%d", blackCnt) );
					showWhiteCnt.setText( String.format("白子：%d", whiteCnt) );
					showStatus.setText( String.format("当前状态：%s回合", enable ? "我方" : "对方") );
				}
			}
		
		}).start();
		
		// 开启线程提示哪一方为先手
		new Thread(new Runnable() {
			public void run() { showDialog(); }
		}).start();
		
	}
	
	private void showDialog() {
		if (enable == true) {
			JOptionPane.showMessageDialog(window, "我方为先手");
		}
		else {
			JOptionPane.showMessageDialog(window, "对方为先手");
		}
	}
	
	// 设置Timer 1000ms实现一次动作 实际是一个线程
	private void setTimer(JLabel time) {
		final JLabel varTime = time;
		long startTime = System.currentTimeMillis();
		Timer timeAction = new Timer(1000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				long timemillis = System.currentTimeMillis();
				// 转换日期显示格式
				SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
				varTime.setText(df.format(new Date(timemillis - startTime - 28800000)));
			}
		});
		timeAction.start();
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		ImageIcon icon = new ImageIcon("src/image/background2.png");
		Image img = icon.getImage();
		g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		
		g.setColor(Color.BLACK);
		for(int i=0; i<15; i++) {
			g.drawLine(distinceOfX, distinceOfY+widthOfLatic*i, widthOfLatic*14+distinceOfX, distinceOfY+widthOfLatic*i);
			g.drawLine(distinceOfX+widthOfLatic*i, distinceOfY, distinceOfX+widthOfLatic*i, widthOfLatic*14 + distinceOfY);
		}
		
		g.fillOval(154, 154, 10, 10);
		g.fillOval(500, 154, 10, 10);
		g.fillOval(154, 498, 10, 10);
		g.fillOval(498, 498, 10, 10);
		g.fillOval(326, 326, 10, 10);
		
		for (int i = 0; i < 15; i++){
			for (int j = 0; j < 15; j++){
				if (board.whichChessInThisPos(i, j) == ChessBoard.Black_Char){		// 画黑子
					int tempX = i*widthOfLatic + distinceOfX;
					int tempY = j*widthOfLatic + distinceOfY;
					g.setColor(Color.BLACK);
					g.fillOval(tempX-10, tempY-10, 25, 25);
				}
				if (board.whichChessInThisPos(i, j) == ChessBoard.White_Char){		// 画白子
					int tempX = i*widthOfLatic + distinceOfX;
					int tempY = j*widthOfLatic + distinceOfY;
					g.setColor(Color.WHITE);
					g.fillOval(tempX-10, tempY-10, 25, 25);
					g.setColor(Color.black);
					g.drawOval(tempX-10, tempY-10, 25, 25);
				}
				if (i == urLastX && j == urLastY) {
					int tempX = i*widthOfLatic + distinceOfX;
					int tempY = j*widthOfLatic + distinceOfY;
					g.setColor(Color.LIGHT_GRAY);
					g.fillOval(tempX-2, tempY-2, 10, 10);
				}
			}
		}
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (gameIsOver == false) {
			if (e.getActionCommand() == "悔棋") {
				if (myLastX != -1 && urLastX != -1) {
					net.send("1,0,0");
					this.enable = false;
				}
				else {
					JOptionPane.showMessageDialog(window, "现在还不能悔棋");
				}
			}
			else if (e.getActionCommand() == "请和") {
				net.send("2,0,0");
				this.enable = false;
			}
			else if (e.getActionCommand() == "投降") {
				net.send("3,0,0");
				this.enable = false;
				gameIsOver = true;
				JOptionPane.showMessageDialog(window, "游戏失败：你举起双手，投降了。");
			}
		}
		else {
			if (e.getActionCommand() == "返回") {
				window.setCurrentPanel(new Welcome(window));
			}
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) { }

	@Override
	public void mousePressed(MouseEvent e) {
		// 获取当前点击位置
		int x = e.getX();
		int y = e.getY();
		
		if (gameIsOver == false && enable && x >= distinceOfX && x <= widthOfLatic*14+distinceOfX+3 && y >= distinceOfY && y <= widthOfLatic*14 + distinceOfY+3) {
			// 转化坐标为棋盘坐标
			double tempx = (x - distinceOfX) / (1.0 * widthOfLatic);
			double tempy = (y - distinceOfY) / (1.0 * widthOfLatic);
			x = (int)(tempx + 0.5);
			y = (int)(tempy + 0.5);
			
			if (board.posIsBlank(x, y)) {
				myLastX = x;
				myLastY = y;
				board.placeChess(isBlack ? ChessBoard.Black_Char : ChessBoard.White_Char, x, y);
				
				if (isBlack) blackCnt ++;
				else	whiteCnt ++;
				
				net.send( String.format("0,%d,%d", x, y) );
				enable = false;
				
				this.repaint();
				
				// 判断游戏是否结束
				if (board.canWin(isBlack ? ChessBoard.Black_Char : ChessBoard.White_Char, x, y)) {
					gameIsOver = true;
				}
			}
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) { }

	@Override
	public void mouseEntered(MouseEvent e) { }

	@Override
	public void mouseExited(MouseEvent e) { }

}
