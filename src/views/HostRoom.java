package views;

import tools.*;
import game.Window;

import java.net.InetAddress;
import java.net.ServerSocket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class HostRoom extends JPanel implements ActionListener {

	NetWork net = null;		// 联机和收发消息的工具
							// 0,xxxx,0     普通信息
							// 1,xxxx,0   一般控制信息
							// 1,游戏开始,0/1	游戏开始控制信息，0/1代表是否为先手
	
	JTextArea show;			// 显示消息
	JTextField input;		// 输入信息
	
	JButton launch;			// 启动房间，启动后服务器才开启
	JButton start;			// 开始游戏 按钮
	JButton exit;			// 关闭        按钮
	JButton send;			// 发送 按钮
	
	boolean connect = false;	// 连接状态
	boolean gameStart = false;
	
	Window window = null;
	
	public HostRoom(Window win) {
		
		this.window = win;
		
		this.setSize(950, 662);										// 设置面板大小
		this.setLayout(null);
		this.setBackground(Color.WHITE);
		
		JLabel label1 = new JLabel("房间信息");
		label1.setFont(new Font("黑体", Font.BOLD, 30));
		label1.setBounds(50, 150, 300, 50);
		this.add(label1);
		
		JLabel label2 = new JLabel(">");
		label2.setFont(new Font("黑体", Font.BOLD, 30));
		label2.setBounds(30, 595, 30, 30);
		this.add(label2);
		
		JPanel p1 = new JPanel(new BorderLayout());
		p1.setBounds(50, 200, 600, 370);
		show = new JTextArea();
		show.setFont(new Font("宋体", Font.BOLD, 22));
		show.setEditable(false);						// 设置文本区不可编辑
		show.setLineWrap(true);        					// 激活自动换行功能
		show.setWrapStyleWord(true);            		// 激活断行不断字功能
		JScrollPane scroll = new JScrollPane(show);		// 添加滚动条
		p1.add(scroll, BorderLayout.CENTER);
		this.add(p1);
		
		input = new JTextField(50);
		input.setFont(new Font("宋体", Font.BOLD, 22));
		input.setBounds(50, 590, 600, 45);
		input.setText("输入信息后点击发送进行聊天");
		this.add(input);
		
		exit = new JButton("离开");
		exit.setBounds(680, 200, 260, 70);
		exit.setFocusPainted(false);
		exit.setFont(new Font("黑体", Font.BOLD, 30));
		exit.setBackground(new Color(0xffededed));
		exit.addActionListener(this);
		this.add(exit);
		
		launch = new JButton("启动房间");
		launch.setBounds(680, 310, 260, 70);
		launch.setFocusPainted(false);
		launch.setFont(new Font("黑体", Font.BOLD, 30));
		launch.setBackground(new Color(0xffededed));
		launch.addActionListener(this);
		this.add(launch);
		
		start = new JButton("开始游戏");
		start.setBounds(680, 420, 260, 70);
		start.setFocusPainted(false);
		start.setFont(new Font("黑体", Font.BOLD, 30));
		start.setBackground(new Color(0xffededed));
		start.addActionListener(this);
		this.add(start);
		start.setEnabled(false);
		
		send = new JButton("发送");
		send.setBounds(680, 530, 260, 70);
		send.setFocusPainted(false);
		send.setFont(new Font("黑体", Font.BOLD, 30));
		send.setBackground(new Color(0xffededed));
		send.addActionListener(this);
		this.add(send);
		send.setEnabled(false);
		
		this.setVisible(true);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		ImageIcon icon = new ImageIcon("src/image/background1.png");
		Image img = icon.getImage();
		g.drawImage(img, 0, 0, 1290, 722, this);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getActionCommand() == "离开") {
			if (connect == true) {
				net.send("1,房主已离开,0");
				net.close();
			}
			window.setCurrentPanel(new Welcome(window));
		}
		else if (e.getActionCommand() == "启动房间") {
			show.append("房间已对外开放\n");
			try {
				String str = new GetWLANip().getIP();
				show.append("IP Address:     " + str + "\n");
				show.append("--------------------------------\n");
				show.setCaretPosition(show.getDocument().getLength());
				launch.setEnabled(false);
				
				net = new NetWork(new ServerSocket(NetWork.PORT));
					
				// 开启线程用于接收房间的消息
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						net.doAccept();
						connect = true;
						send.setEnabled(true);
						
						while (!gameStart) {
							try{ Thread.sleep(30); } catch (Exception e) { };
							String []info = net.receive().split(",");
							if (info[0].equals("1")) {
								if (info[1].equals("玩家已准备")) {
									show.append("[系统消息]" + info[1] + "\n");
									show.setCaretPosition(show.getDocument().getLength());
									start.setEnabled(true);
								}
								else if (info[1].equals("玩家已离开")) {
									show.append("[系统消息]" + info[1] + "\n");
									show.setCaretPosition(show.getDocument().getLength());
									net.close();
									break;
								}
								else if (info[1].equals("玩家取消准备")) {
									show.append("[系统消息]" + info[1] + "\n");
									show.setCaretPosition(show.getDocument().getLength());
									start.setEnabled(false);
								}
								else if (info[1].equals("确认游戏开始")) {
									break;
								}
								else {
									show.append("[系统消息]" + info[1] + "\n");
									show.setCaretPosition(show.getDocument().getLength());
								}
							}
							else if (info[0].equals("0")) {
								show.append("-- " + info[1] + "\n");
								show.setCaretPosition(show.getDocument().getLength());
							}
						}
					}
					
				}).start();
				
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		else if (e.getActionCommand() == "开始游戏") {
			int randInt = (int)(0 + Math.random() * (1 - 0 + 1));
			int sendInt = (randInt == 1) ? 0 : 1;
			
			net.send( String.format("1,游戏开始,%d", sendInt) );
			gameStart = true;
			
			window.setCurrentPanel(new GameView(window, net, (randInt == 1) ? true : false));
		}
		else if (e.getActionCommand() == "发送") {
			String str = input.getText();
			if (!str.equals("")) {
				input.setText("");
				net.send("0," + str + ",0");
				show.append(">> " + str + "\n");
				show.setCaretPosition(show.getDocument().getLength());
			}
		}
	}

}
