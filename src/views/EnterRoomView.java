package views;

import tools.*;
import game.Window;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class EnterRoomView extends JPanel implements ActionListener{

	NetWork net;			// 联机和收发消息的工具
	
	JButton enter;			// 进入 按钮
	JButton back;			// 返回 按钮
	JButton cancel;			// 取消 按钮
	
	JTextField input;		// 输入框，输入房间IP
	
	Window window = null;
	
	public EnterRoomView(Window win) {
		
		this.window = win;
		
		this.setSize(950, 662);										// 设置面板大小
		this.setLayout(null);
		this.setBackground(Color.WHITE);
	
		back = new JButton("返回");
		back.setBounds(30, 30, 150, 50);
		back.setFocusPainted(false);
		back.setFont(new Font("黑体", Font.BOLD, 25));
		back.setBackground(new Color(0xffededed));
		back.addActionListener(this);
		this.add(back);
		
		JLabel label = new JLabel("房间IP:");
		label.setFont(new Font("黑体", Font.BOLD + Font.ITALIC, 45));
		label.setBounds(750, 240, 300, 60);
		this.add(label);
		
		input = new JTextField(50);
		input.setFont(new Font("黑体", Font.PLAIN, 45));
		input.setHorizontalAlignment(JTextField.RIGHT);
		input.setBounds(300, 300, 620, 65);
		this.add(input);
		
		enter = new JButton("进入");
		enter.setBounds(490, 450, 430, 90);
		enter.setFocusPainted(false);
		enter.setFont(new Font("黑体", Font.BOLD, 40));
		enter.setBackground(new Color(0xffededed));
		enter.addActionListener(this);
		this.add(enter);
		
		cancel = new JButton("取消");
		cancel.setBounds(40, 450, 430, 90);
		cancel.setFocusPainted(false);
		cancel.setFont(new Font("黑体", Font.BOLD, 40));
		cancel.setBackground(new Color(0xffededed));
		cancel.addActionListener(this);
		this.add(cancel);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		ImageIcon icon = new ImageIcon("src/image/background1.png");
		Image img = icon.getImage();
		g.drawImage(img, 0, 0, 1290, 722, this);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getActionCommand() == "返回") {
			window.setCurrentPanel(new Welcome(window));
		}
		else if (e.getActionCommand() == "取消") {
			window.setCurrentPanel(new Welcome(window));
		}
		else if (e.getActionCommand() == "进入") {
			
			try {
				
				String str = input.getText();
				net = new NetWork(new Socket(str, NetWork.PORT));
				net.send("1," + new GetWLANip().getIP() + "已进入,0");
				
				window.setCurrentPanel(new Room(window, net));
			
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		}
	}
	
}
