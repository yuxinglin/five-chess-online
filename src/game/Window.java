package game;

import views.*;
import java.awt.*;
import javax.swing.*;

public class Window extends JFrame {

	JPanel currentPanel = null;
	
	public Window() {
		this.setTitle("��Ѫ������  v2.1.3");
		this.setSize(980, 710);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		
		int width = Toolkit.getDefaultToolkit().getScreenSize().width;  	// ��Ļ��
		int height = Toolkit.getDefaultToolkit().getScreenSize().height; 	// ��Ļ��
		
		this.setLocation((width - 980) / 2, (height-710) / 3);				// ���ô���λ��
		
		currentPanel = new Welcome(this);
		this.add(currentPanel, BorderLayout.CENTER);
		
		ImageIcon icon = new ImageIcon("src/image/Icon1.png");
		setIconImage(icon.getImage());
		
		this.setVisible(true);
	}
	
	public void setCurrentPanel(JPanel panel) {
		this.remove(currentPanel);
		currentPanel = panel;
		this.add(currentPanel, BorderLayout.CENTER);
		this.revalidate();
		this.repaint();
	}
	
}
