package tools;

import java.net.*;
import java.awt.Font;
import java.io.*;

public class NetWork {

	public static int PORT = 9999;
	
	ServerSocket ss;
	Socket s;
	BufferedReader reader;
	PrintWriter writer;
	
	public NetWork(ServerSocket paramSS) {
		ss = paramSS;
	}
	
	public NetWork(Socket paramS) {
		s = paramS;
		try {
			reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
			writer = new PrintWriter(s.getOutputStream(), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// 作为服务器端工具侦听
	public void doAccept() {
		try {
			// 此语句会阻塞进程
			s = ss.accept();
			reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
			writer = new PrintWriter(s.getOutputStream(), true);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// 发送消息
	public void send(String str) {
		writer.println(str);
	}
	
	// 接收消息
	public String receive() {
		String str = "";
		try {
			str = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str;
	}
	
	// 关闭连接
	public void close() {
		try {
			
			s.close();
			writer.close();
			reader.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
