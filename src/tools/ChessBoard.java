package tools;

public class ChessBoard {
	
	public static char White_Char = 'O';	// 白棋
	public static char Black_Char = 'X'; 	// 黑棋
	public static char Blank_Char = ' ';	// 空位
	
	public static int M = 15;				// 棋盘规格 15*15
	public static int GridNumber = 225;		// 一维棋盘长度
	
	char boardInOneDimens[];
	
	public ChessBoard() {
		boardInOneDimens = new char[ChessBoard.GridNumber];
		for (int i = 0; i < ChessBoard.GridNumber; i ++) {
			boardInOneDimens[i] = ChessBoard.Blank_Char;
		}
	}
	
	// 判断位置 (x, y) 是否是空位
	public boolean posIsBlank(int x, int y) {
		return boardInOneDimens[x * ChessBoard.M + y] == ChessBoard.Blank_Char;
	}
	
	// 判断棋盘是否已经填满
	public boolean isFull() {
		for (int i = 0; i < ChessBoard.GridNumber; i ++) {
			if (boardInOneDimens[i] == ChessBoard.Blank_Char)
				return false;
		}
		return true;
	}
	
	// 当前位置是什么棋子
	public char whichChessInThisPos(int x, int y) {
		return boardInOneDimens[x * ChessBoard.M + y];
	}
	
	// 判断当前所放旗子是否能造成下棋者赢
	public boolean canWin(char c, int x, int y) {
		int i = 1, cnt = 1;
		// 向右
		while (y + i < ChessBoard.M && boardInOneDimens[x * ChessBoard.M + y + i] == c) { i ++; cnt ++; }
		// 向左
		i = 1;
		while (y - i >= 0 && boardInOneDimens[x * ChessBoard.M + y - i] == c) { i ++; cnt ++; }
		if (cnt >= 5) return true;
		
		// 向下
		i = 1; cnt = 1;
		while (x + i < ChessBoard.M && boardInOneDimens[(x + i) * ChessBoard.M + y] == c) { i ++; cnt ++; }
		// 向上
		i = 1;
		while (x - i >= 0 && boardInOneDimens[(x - i) * ChessBoard.M + y] == c) { i ++; cnt ++; }
		if (cnt >= 5) return true;
		
		// 斜右向下
		i = 1; cnt = 1;
		while (x + i < ChessBoard.M && y + i < ChessBoard.M && boardInOneDimens[(x + i) * ChessBoard.M + y + i] == c) { i ++; cnt ++; }
		// 斜左向上
		i = 1;
		while (x - i >= 0 && y - i >= 0 && boardInOneDimens[(x - i) * ChessBoard.M + y - i] == c) { i ++; cnt ++; }
		if (cnt >= 5) return true;
		
		// 斜左向下
		i = 1; cnt = 1;
		while (x + i < ChessBoard.M && y - i >= 0 && boardInOneDimens[(x + i) * ChessBoard.M + y - i] == c) { i ++; cnt ++; }
		// 斜右向上
		i = 1;
		while (x - i >= 0 && y + i < ChessBoard.M && boardInOneDimens[(x - i) * ChessBoard.M + y + i] == c) { i ++; cnt ++; }
		if (cnt >= 5) return true;
		
		return false;
	}
	
	// 放置棋子
	public void placeChess(char c, int x, int y) {
		boardInOneDimens[x * ChessBoard.M + y] = c;
	}
	
	// 清空位置
	public void unPlace(int x, int y) {
		boardInOneDimens[x * ChessBoard.M + y] = ChessBoard.Blank_Char;
	}
	
	// 打印棋盘
	public void print() {
		for (int i = 0; i < ChessBoard.M; i ++) {
			for (int j = 0; j < ChessBoard.M; j ++) {
				System.out.print(boardInOneDimens[i * ChessBoard.M + j] + " ");
			}
			System.out.println("");
		}
	}
	
}
