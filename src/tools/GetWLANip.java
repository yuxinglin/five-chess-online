package tools;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class GetWLANip {
	
	private boolean isWiFiConnected=false;
	private String myIP=null;//本机的WiFi ip
	private String connecterIP=null;//本机连接的路由的IP
	private String connecterMAC=null;//本机连接的路由的MAC
	private boolean isConnecterHDCP=false;//本机连接的路由是否为HDCP动态
	
	public GetWLANip() {
		//通过ipconfig获取WLAN的IP地址
		String str = Cmd.command("ipconfig");
		str = str+"\r\n";
		
		//获取“无线局域网适配器WLAN”段
		String WLAN_str = null;
		Pattern pattern = Pattern.compile("无线局域网适配器 WLAN:\r\n\r\n[\\s\\S]*?\r\n\r\n");//设置正则表达形式，[\\s\\S]*?为任意数量字符非贪婪正则表达式
		Matcher matcher = pattern.matcher(str);//对str进行正则表达式搜索
		if(matcher.find()){
			int start = matcher.start();
		    int end = matcher.end();
		    WLAN_str=str.substring(start,end);
		}
		if(WLAN_str==null) {
			return;
		}
		
		str=Cmd.command("ARP -a");
		str=str+"\r\n";//增加用于确定文字分段的回车
 
		//获取myIP
		pattern = Pattern.compile("IPv4 地址 . . . . . . . . . . . . : ");//设置正则表达形式，[\\s\\S]*?为任意数量字符非贪婪正则表达式
		matcher = pattern.matcher(WLAN_str);//对str进行正则表达式搜索
		if(matcher.find()){
		    int end = matcher.end();
		    myIP=WLAN_str.substring(end).split("\r\n")[0];//首先截取匹配到的字符串，然后读到回车，获取ip地址
		    isWiFiConnected=true;
		}
		else {
			isWiFiConnected=false;
			return;
		}
		
		//System.out.println(myIP);
	}
	
	public String getIP() {
		return myIP;
	}
}

class Cmd {
	public static String command(String cmd){//获取cmd输出
		try {
			Process process = Runtime.getRuntime().exec(cmd);
			
			//关闭流释放资源
	    	if(process != null){
	    		process.getOutputStream().close();
	    	}
	    	InputStream in = process.getInputStream();
	    	BufferedReader br = new BufferedReader(new InputStreamReader(in));
	    	
	    	StringBuilder result = new StringBuilder();
	    	String tmp = null;
	    	while ((tmp = br.readLine()) != null) {
	    		result.append(tmp+"\r\n");//将tmp内容附加（append）到StringBuilder类中
	    	}
	    	return result.toString();
	    	
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
			return null;
		}
	}
}
